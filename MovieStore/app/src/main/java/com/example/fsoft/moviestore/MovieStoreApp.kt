package com.example.fsoft.moviestore

import android.app.Activity
import android.app.Application
import com.example.fsoft.moviestore.di.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.DaggerApplication
import dagger.android.HasActivityInjector
import javax.inject.Inject

/**
 * Created by nguyennv on 3/28/2018 AD.
 */
class MovieStoreApp: Application(), HasActivityInjector {


    @Inject
    lateinit var applicationDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

//    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
//        return applicationDispatchingAndroidInjector
//    }
override fun activityInjector(): AndroidInjector<Activity> {
    return applicationDispatchingAndroidInjector
}
    override fun onCreate() {
        super.onCreate()

        DaggerAppComponent.builder()
                .application(this)
                .build()
                .inject(this)

    }
}