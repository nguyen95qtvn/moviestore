/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.example.fsoft.moviestore.managers.local.db

import com.example.fsoft.moviestore.models.PopularMovie
import java.util.concurrent.Callable

import javax.inject.Inject
import javax.inject.Singleton

import io.reactivex.Observable

@Singleton
class AppDbHelper @Inject constructor(mAppDatabase: AppDatabase) : DbHelper {


    override fun getAllFavMovie(): Observable<List<PopularMovie>> {
        return Observable.fromCallable{ mAppDatabase.movieDAO().getAllMovie()}
    }

    override fun getFavMovie(id: Int): Observable<PopularMovie> {
        return Observable.fromCallable { mAppDatabase.movieDAO().findMovieByID(id)}
    }

    override fun deleteAllFavMovie() {
        this.mAppDatabase.movieDAO().deleteAll()
    }

    private var mAppDatabase: AppDatabase = mAppDatabase


    override fun deleteMovie(popularMovie: PopularMovie) {
        this.mAppDatabase.movieDAO().delete(popularMovie)
    }

    override fun addFavMovie(popularMovie: PopularMovie) {
        mAppDatabase.movieDAO().insert(popularMovie)
    }

}
