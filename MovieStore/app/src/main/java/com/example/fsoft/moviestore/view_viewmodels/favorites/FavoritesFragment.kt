package com.example.fsoft.moviestore.view_viewmodels.favorites

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.fsoft.moviestore.BR

import com.example.fsoft.moviestore.R
import com.example.fsoft.moviestore.ViewModelProviderFactory
import com.example.fsoft.moviestore.databinding.FragmentFavoritesBinding
import com.example.fsoft.moviestore.managers.AppDataManager
import com.example.fsoft.moviestore.managers.DataManager
import com.example.fsoft.moviestore.models.PopularMovie
import com.example.fsoft.moviestore.view_viewmodels.base.BaseFragment
import com.example.fsoft.moviestore.view_viewmodels.movies.MoviesFragment
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [FavoritesFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [FavoritesFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class FavoritesFragment: BaseFragment<FragmentFavoritesBinding, FavoritesViewModel>(), FavoritesNavigator, FavoritesAdapter.FavoritesApdapterListener {

    @Inject
    lateinit var mFavoritesAdapter: FavoritesAdapter

    private lateinit var mFragmentFavoritesBinding: FragmentFavoritesBinding

    @Inject
    lateinit var mLayoutManager: LinearLayoutManager

    @Inject
    lateinit var mViewModelFactory: ViewModelProvider.Factory

    lateinit var mFavoritesViewModel: FavoritesViewModel

    var mListener: FavoritesFragment.OnFragmentInteractionListener? = null


    override fun handleError(throwable: Throwable) {
        print("Error: $throwable")
    }

    override fun updateFavorites(movieList: List<PopularMovie>) {
        mFavoritesAdapter.addAll(movieList)
    }

    override fun onRetryClick() {

    }

    override fun getBindingVariable(): Int {
        return BR.viewModel
    }
    @LayoutRes
    override fun getLayoutId(): Int {
        return R.layout.fragment_favorites
    }

    override fun getViewModel(): FavoritesViewModel {
        mFavoritesViewModel = ViewModelProviders.of(this,mViewModelFactory).get(FavoritesViewModel::class.java)
        return mFavoritesViewModel
        }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mFavoritesViewModel!!.navigator = this
        mFavoritesAdapter.mListener = this
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        mFragmentFavoritesBinding = viewDataBinding!!
        setUp()
        subscribeToLiveData()
        return view
    }

    fun setUp() {
        mLayoutManager.orientation = LinearLayoutManager.VERTICAL
        mFragmentFavoritesBinding.favoritesRecyclerView.layoutManager = mLayoutManager
        mFragmentFavoritesBinding.favoritesRecyclerView.itemAnimator = DefaultItemAnimator()
        mFragmentFavoritesBinding.favoritesRecyclerView.adapter = mFavoritesAdapter
    }

    private fun subscribeToLiveData() {
        mFavoritesViewModel!!.movieListLiveData.observe(this, Observer {
            movies -> mFavoritesViewModel!!.addFavoritesToList(movies!!)
        })
//        mFavoritesViewModel!!.movieListLiveData.observeForever { movies -> mFavoritesViewModel!!.addFavoritesToList(movies!!) }
    }

    companion object {
        fun newInstance(): FavoritesFragment {
            val args = Bundle()
            val fragment = FavoritesFragment()
            fragment.arguments = args
            return fragment
        }
    }
    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }
}
