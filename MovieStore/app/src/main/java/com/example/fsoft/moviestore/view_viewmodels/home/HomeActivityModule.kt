package com.example.fsoft.moviestore.view_viewmodels.home

import android.arch.lifecycle.ViewModelProvider
import com.example.fsoft.moviestore.ViewModelProviderFactory
import com.example.fsoft.moviestore.managers.DataManager
import com.example.fsoft.moviestore.utils.rx.SchedulerProvider
import dagger.Module
import dagger.Provides

@Module
class HomeActivityModule {
    @Provides
    fun provideViewModelProviderFactory(dataManager: DataManager,schedulerProvider: SchedulerProvider) : HomeViewModel {
        return HomeViewModel(dataManager,schedulerProvider)
    }
}