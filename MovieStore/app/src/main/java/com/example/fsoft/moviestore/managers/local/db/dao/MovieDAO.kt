package com.example.fsoft.moviestore.managers.local.db.dao

import android.arch.persistence.room.*
import com.example.fsoft.moviestore.models.PopularMovie

/**
 * Created by nguyennv on 3/27/2018 AD.
 */
@Dao
interface MovieDAO {
    @Insert
    fun insert(popularMovie: PopularMovie)

    @Delete
    fun delete(popularMovie: PopularMovie)

    @Update
    fun update(popularMovie: PopularMovie)

    @Query("SELECT * FROM FavoritesMovie")
    fun getAllMovie() : List<PopularMovie>

    @Query("DELETE FROM FavoritesMovie")
    fun deleteAll()

    @Query("SELECT * FROM FavoritesMovie WHERE movieID = :id")
    fun findMovieByID(id: Int): PopularMovie
}