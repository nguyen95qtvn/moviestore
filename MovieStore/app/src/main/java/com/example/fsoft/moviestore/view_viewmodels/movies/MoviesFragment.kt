package com.example.fsoft.moviestore.view_viewmodels.movies

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast

import com.example.fsoft.moviestore.R
import com.example.fsoft.moviestore.models.PagePopularMovie
import com.example.fsoft.moviestore.models.PopularMovie
import com.example.fsoft.moviestore.services.APIService
import com.example.fsoft.moviestore.services.APIUtils
import com.example.fsoft.moviestore.view_viewmodels.home.HomeActivity
import kotlinx.android.synthetic.main.fragment_movies.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [MoviesFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [MoviesFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MoviesFragment : Fragment() {
    private var mListener: OnFragmentInteractionListener? = null
    private var homeActivity: HomeActivity? = null
    private var apiService: APIService? = null
    private var mAdapter: MovieAdapter? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_movies, container, false)

        apiService = APIUtils.getSOService()
        loadData()



        return view
    }

    public fun loadData(){


        apiService!!.getListMovie().enqueue(object : Callback<PagePopularMovie> {
            override fun onResponse(call: Call<PagePopularMovie>?, response: Response<PagePopularMovie>?) {
                if(response!!.isSuccessful()){

//                    val rv = view!!.findViewById<RecyclerView>(R.id.listview_movie)
//                    rv.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
                   var listMovie: List<PopularMovie> = response.body().results!!
//                   for (item in listMovie){
//                       Log.d("Test",item.title)
//                   }
                    val p = ArrayList<PopularMovie>(listMovie)
                    mAdapter = MovieAdapter(p)
//
//                    listview_movie.adapter = mAdapter
                    val layoutManager = LinearLayoutManager(activity)
                    listview_movie.setLayoutManager(layoutManager)
                    listview_movie.setAdapter(mAdapter)
//                    listview_movie.setHasFixedSize(true)
//                    val itemDecoration = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
//                    listview_movie.addItemDecoration(itemDecoration)
                }
            }

            override fun onFailure(call: Call<PagePopularMovie>?, t: Throwable?) {
                Toast.makeText(context, "Fail", Toast.LENGTH_SHORT).show()
            }

        })
    }


    fun onButtonPressed(uri: Uri) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(uri)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }


    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }
}// Required empty public constructor
