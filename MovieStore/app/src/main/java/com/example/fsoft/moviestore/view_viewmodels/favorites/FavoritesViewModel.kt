package com.example.fsoft.moviestore.view_viewmodels.favorites

import android.arch.lifecycle.MutableLiveData
import android.databinding.ObservableArrayList
import android.databinding.ObservableList
import android.graphics.ColorSpace
import com.example.fsoft.moviestore.managers.DataManager
import com.example.fsoft.moviestore.models.PopularMovie
import com.example.fsoft.moviestore.utils.rx.SchedulerProvider
import com.example.fsoft.moviestore.view_viewmodels.base.BaseViewModel
import javax.inject.Inject
import javax.security.auth.callback.Callback

/**
 * Created by nguyennv on 3/27/2018 AD.
 */
class FavoritesViewModel @Inject constructor(dataManager: DataManager, schedulerProvider: SchedulerProvider): BaseViewModel<FavoritesNavigator>(dataManager,schedulerProvider) {

    val movieObservableList: ObservableList<PopularMovie> = ObservableArrayList<PopularMovie>() as ObservableList<PopularMovie>

    val movieListLiveData: MutableLiveData<List<PopularMovie>> = MutableLiveData()

    init {
        fetchFavorites()
    }

    fun addFavoritesToList(listMovie: List<PopularMovie>) {
        movieObservableList.clear()
        movieObservableList.addAll(listMovie)
    }

    private fun fetchFavorites() {
        setIsLoading(true)
        compositeDisposable.add(
                dataManager.getMovieDetail()
                        .subscribeOn(schedulerProvider.io())
                        .observeOn(schedulerProvider.ui())
                        .subscribe({ movie ->
                            if (movie != null) {
                                movieListLiveData.value = movie
                            }
                            setIsLoading(false)
                        }) { throwable ->
                            setIsLoading(false)
                            navigator!!.handleError(throwable)
                        }
        )
    }
}