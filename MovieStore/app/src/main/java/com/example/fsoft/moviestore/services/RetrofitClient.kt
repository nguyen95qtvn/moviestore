package com.example.fsoft.moviestore.services

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by HuuMX1 on 3/20/2018.
 */

object RetrofitClient {
    private var retrofit: Retrofit? = null
    fun getClient(baseURL: String): Retrofit {
        if (retrofit == null) {
            retrofit = Retrofit.Builder()
                    .baseUrl(baseURL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
        }
        return retrofit!!
    }
}
