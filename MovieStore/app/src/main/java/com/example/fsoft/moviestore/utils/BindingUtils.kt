package com.example.fsoft.moviestore.utils

import android.content.Context
import android.databinding.BindingAdapter
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.widget.ImageView

import com.example.fsoft.moviestore.models.PopularMovie
import com.example.fsoft.moviestore.view_viewmodels.favorites.FavoritesAdapter
import com.squareup.picasso.Picasso
import retrofit2.http.Url
import java.net.URI


object BindingUtils {
// This class is not publicly instantiable

    @BindingAdapter("adapter")
    @JvmStatic
    fun addFavoriteItems(recyclerView: RecyclerView, items : List<PopularMovie>) {
        if (recyclerView.adapter is FavoritesAdapter) {
            var adapter: FavoritesAdapter = recyclerView.adapter as FavoritesAdapter
            adapter.clearAll()
            adapter.addAll(items)
        }

    }
    @BindingAdapter("imageUrl")
    @JvmStatic
    fun setImageUrl(imageView: ImageView, url: String?) {
        if (url != null) {
            Picasso.get().load("https://image.tmdb.org/t/p/w92$url").into(imageView)
        }
    }
}