package com.example.fsoft.moviestore.view_viewmodels.favorites

import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by nguyennv on 3/27/2018 AD.
 */
@Module
abstract class FavoritesFragmentProvider {

    @ContributesAndroidInjector(modules = [(FavoritesFragmentModule::class)])
    abstract fun provideFavoritesFragmentFactory(): FavoritesFragment
}