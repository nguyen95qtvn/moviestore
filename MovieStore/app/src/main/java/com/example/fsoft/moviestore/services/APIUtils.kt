package com.example.fsoft.moviestore.services

/**
 * Created by HuuMX1 on 3/20/2018.
 */

object APIUtils {
    //public static final String BASE_URL = "http://dev.androidcoban.com/blog/";
    //public static final String BASE_URL = "https://api.stackexchange.com/2.2/";
    private const val BASE_URL = "http://api.themoviedb.org/"
    fun getSOService(): APIService{
        return RetrofitClient.getClient(BASE_URL).create(APIService::class.java)
    }

}
