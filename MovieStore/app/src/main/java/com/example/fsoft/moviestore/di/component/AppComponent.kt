package com.example.fsoft.moviestore.di.component

import android.app.Application
import com.example.fsoft.moviestore.MovieStoreApp
import com.example.fsoft.moviestore.di.builder.ActivityBuilder
import com.example.fsoft.moviestore.di.module.AppModule
import com.example.fsoft.moviestore.view_viewmodels.favorites.FavoritesFragmentProvider
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjection
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Created by nguyennv on 3/28/2018 AD.
 */
@Singleton
@Component(modules = [(AndroidSupportInjectionModule::class), (AppModule::class), (ActivityBuilder::class)])
interface AppComponent {

    fun inject(movieStoreApp: MovieStoreApp)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}