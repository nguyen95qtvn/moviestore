package com.example.fsoft.moviestore.view_viewmodels.home

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.databinding.ViewDataBinding
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v4.view.ViewPager
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import com.example.fsoft.moviestore.R
import com.example.fsoft.moviestore.view_viewmodels.about.AboutFragment
import com.example.fsoft.moviestore.view_viewmodels.favorites.FavoritesFragment

import com.example.fsoft.moviestore.view_viewmodels.setting.SettingFragment
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.app_bar_home.*
import kotlinx.android.synthetic.main.content_home.*
import android.view.View.OnTouchListener
import com.example.fsoft.moviestore.BR
import com.example.fsoft.moviestore.view_viewmodels.base.BaseActivity
import com.example.fsoft.moviestore.view_viewmodels.movies.MoviesFragment
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject


class HomeActivity : BaseActivity<ViewDataBinding, HomeViewModel>(), NavigationView.OnNavigationItemSelectedListener,AboutFragment.OnFragmentInteractionListener, SettingFragment.OnFragmentInteractionListener,FavoritesFragment.OnFragmentInteractionListener, MoviesFragment.OnFragmentInteractionListener, HasSupportFragmentInjector{

    @Inject
    lateinit var mHomeViewModel: HomeViewModel

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, HomeActivity::class.java)
        }
    }


    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_home
    }

    override fun getViewModel(): HomeViewModel {
        return mHomeViewModel
    }

    @Inject
    lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return fragmentDispatchingAndroidInjector
    }

    override fun onFragmentInteraction(uri: Uri) {

    }


    val tabs = arrayOf("Movies","Favorites","Settings","About")
    val resourceIds = arrayOf(R.layout.fragment_movies, R.layout.fragment_favorites,R.layout.fragment_setting, R.layout.fragment_about)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
        setupViewpager(viewPager)
        tabLayout.setupWithViewPager(viewPager)

    }

    private fun setupViewpager(viewPager: ViewPager){
        var adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(MoviesFragment(),"Movies")
        adapter.addFragment(FavoritesFragment.newInstance(),"Favorites")
        adapter.addFragment(SettingFragment(),"Settings")
        adapter.addFragment(AboutFragment(),"About")
        viewPager.adapter = adapter
        viewPager.offscreenPageLimit = 4

        //viewPager.setOnTouchListener(OnTouchListener { v, event -> true })
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_camera -> {
                // Handle the camera action
            }
            R.id.nav_gallery -> {

            }
            R.id.nav_slideshow -> {

            }
            R.id.nav_manage -> {

            }
            R.id.nav_share -> {

            }
            R.id.nav_send -> {

            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

}
