package com.example.fsoft.moviestore.view_viewmodels.home

import com.example.fsoft.moviestore.managers.DataManager
import com.example.fsoft.moviestore.utils.rx.SchedulerProvider
import com.example.fsoft.moviestore.view_viewmodels.base.BaseViewModel
import javax.inject.Inject

class HomeViewModel
@Inject
constructor(dataManager: DataManager, schedulerProvider: SchedulerProvider) : BaseViewModel<HomeNavigator>(dataManager, schedulerProvider) {


}
