package com.example.fsoft.moviestore.view_viewmodels.home

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import javax.net.ssl.TrustManager

/**
 * Created by nguyennv on 3/23/18.
 */
class ViewPagerAdapter(manager: FragmentManager): FragmentPagerAdapter(manager) {
    var mFragmentList = ArrayList<Fragment>()
    var mFragmentTitle = ArrayList<String>()

    override fun getItem(position: Int): Fragment {
        return mFragmentList.get(position)
    }

    override fun getCount(): Int {
        return mFragmentList.size
    }
    fun addFragment(fragment: Fragment, title: String){
        mFragmentList.add(fragment)
        mFragmentTitle.add(title)
    }
    override fun getPageTitle(position: Int): CharSequence {
        return mFragmentTitle.get(position)
    }

}

