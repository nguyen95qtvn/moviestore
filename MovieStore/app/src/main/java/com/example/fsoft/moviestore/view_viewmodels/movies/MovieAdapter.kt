package com.example.fsoft.moviestore.view_viewmodels.movies

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import com.example.fsoft.moviestore.models.PopularMovie
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.fsoft.moviestore.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_home.view.*

/**
 * Created by HuuMX1 on 3/27/2018.
 */
class MovieAdapter(val movieList: ArrayList<PopularMovie>): RecyclerView.Adapter<MovieAdapter.ViewHolder>(){
    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.txt_title?.text = movieList[position].title
        holder?.txt_date?.text = movieList[position].releaseDate
        holder?.txt_rating?.text = movieList[position].voteAverage.toString()
        holder?.txt_overview?.text = movieList[position].overview
        Picasso.get().load("https://image.tmdb.org/t/p/w92"+movieList[position].posterPath).into(holder?.img_poster)

        if(movieList[position].adult!!){
            holder?.txt_label?.visibility = View.VISIBLE
        }
        else{
            holder?.txt_label?.visibility = View.INVISIBLE
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent?.context).inflate(R.layout.item_list_movie, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return movieList.size
    }


    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val txt_title = itemView.findViewById<TextView>(R.id.tv_title)
        val img_poster = itemView.findViewById<ImageView>(R.id.img_poster)
        val txt_date = itemView.findViewById<TextView>(R.id.tv_date)
        val txt_rating = itemView.findViewById<TextView>(R.id.tv_rating)
        val txt_overview = itemView.findViewById<TextView>(R.id.tv_overview)
        val txt_label = itemView.findViewById<TextView>(R.id.tv_label)
    }
}


