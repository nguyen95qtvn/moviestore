package com.example.fsoft.moviestore.services

import com.example.fsoft.moviestore.models.PagePopularMovie
import com.example.fsoft.moviestore.models.PopularMovie

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by HuuMX1 on 3/20/2018.
 */

interface APIService {
    @GET("/3/movie/popular?api_key=e7631ffcb8e766993e5ec0c1f4245f93&page={1}")
    fun getMovieDetail(): Call<List<PopularMovie>>

    @GET("/3/movie/popular?api_key=e7631ffcb8e766993e5ec0c1f4245f93&page={1}")
    fun getListMovie(): Call<PagePopularMovie>

}