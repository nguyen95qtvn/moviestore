/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.example.fsoft.moviestore.utils


object AppConstants {

    const val API_STATUS_CODE_LOCAL_ERROR = 0

    const val DB_NAME = "movies_store_db"

    const val NULL_INDEX = -1L

    const val PREF_NAME = "movies_pref"

    const val SEED_DATABASE_OPTIONS = "seed/options.json"

    const val SEED_DATABASE_QUESTIONS = "seed/questions.json"

    const val STATUS_CODE_FAILED = "failed"

    const val STATUS_CODE_SUCCESS = "success"

    const val TIMESTAMP_FORMAT = "yyyyMMdd_HHmmss"
}// This utility class is not publicly instantiable
