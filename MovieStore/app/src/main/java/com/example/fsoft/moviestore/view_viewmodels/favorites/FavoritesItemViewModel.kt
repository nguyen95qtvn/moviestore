package com.example.fsoft.moviestore.view_viewmodels.favorites

import android.databinding.ObservableField
import com.example.fsoft.moviestore.models.PopularMovie


/**
 * Created by nguyennv on 3/27/2018 AD.
 */
class FavoritesItemViewModel(val movie: PopularMovie, val mListener: FavoritesItemViewModelListener) {

    val movieName: ObservableField<String>
    val releaseDate: ObservableField<String>
    val rating: ObservableField<Double>
    val overview: ObservableField<String>
    val imageUrl: ObservableField<String>

    init {
        movieName = ObservableField(movie.title!!)
        releaseDate = ObservableField(movie.releaseDate.toString())
        rating = ObservableField(movie.voteAverage!!)
        overview = ObservableField(movie.overview!!)
        imageUrl = ObservableField(movie.posterPath!!)
    }
    fun onItemClick() {
        mListener.onItemClick(movie)
    }

    interface FavoritesItemViewModelListener {

        fun onItemClick(popularMovie: PopularMovie)
    }
}