

package com.example.fsoft.moviestore.managers.remote

import android.util.Log
import com.example.fsoft.moviestore.models.PagePopularMovie
import com.example.fsoft.moviestore.models.PopularMovie
import com.example.fsoft.moviestore.services.APIService
import com.example.fsoft.moviestore.services.APIUtils
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.ObservableOnSubscribe
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class AppApiHelper : ApiHelper {
    private var mApiService: APIService = APIUtils.getSOService()
    private var mListMovie: List<PopularMovie>? = null

    @Inject constructor() {
    }

    override fun getMovieDetail(): Observable<List<PopularMovie>> {

        return Observable.create(ObservableOnSubscribe { e: ObservableEmitter<List<PopularMovie>> ->
            run {
                var call = mApiService.getListMovie()
                var result = call.execute().body().results
                if (result == null) {
                    e.onError(Throwable("service error"))
                } else {
                    e.onNext(result)
                }
                e.onComplete()
            }
        })
    }

    override fun getListMovie(): Observable<PagePopularMovie> {

        return Observable.just(PagePopularMovie())

    }
}
