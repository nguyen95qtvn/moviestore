package com.example.fsoft.moviestore.view_viewmodels.favorites

import android.arch.lifecycle.ViewModelProvider
import android.support.v7.widget.LinearLayoutManager
import com.example.fsoft.moviestore.ViewModelProviderFactory
import com.example.fsoft.moviestore.managers.DataManager
import com.example.fsoft.moviestore.models.PopularMovie
import com.example.fsoft.moviestore.utils.rx.SchedulerProvider
import dagger.Module
import dagger.Provides
import java.util.ArrayList

/**
 * Created by nguyennv on 3/27/2018 AD.
 */
@Module
class FavoritesFragmentModule {
    @Provides
    fun provideFavoritesViewModel(dataManager: DataManager,
                                    schedulerProvider: SchedulerProvider): FavoritesViewModel {
        return FavoritesViewModel(dataManager, schedulerProvider)
    }

    @Provides
    fun provideFavoritesAdapter(): FavoritesAdapter {
        return FavoritesAdapter(ArrayList<PopularMovie>())
    }

    @Provides
    fun provideLinearLayoutManager(fragment: FavoritesFragment): LinearLayoutManager {
        return LinearLayoutManager(fragment.activity!!)
    }

    @Provides
    fun provideFavoritesFactory(favoritesViewModel: FavoritesViewModel): ViewModelProvider.Factory {
        return ViewModelProviderFactory<FavoritesViewModel>(favoritesViewModel)
    }

}