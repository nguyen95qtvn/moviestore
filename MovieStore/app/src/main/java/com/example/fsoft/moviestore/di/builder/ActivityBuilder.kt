package com.example.fsoft.moviestore.di.builder

import com.example.fsoft.moviestore.view_viewmodels.favorites.FavoritesFragment
import com.example.fsoft.moviestore.view_viewmodels.favorites.FavoritesFragmentProvider
import com.example.fsoft.moviestore.view_viewmodels.home.HomeActivity
import com.example.fsoft.moviestore.view_viewmodels.home.HomeActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by nguyennv on 3/28/2018 AD.
 */
@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [(FavoritesFragmentProvider::class),(HomeActivityModule::class)])
    abstract fun bindHomeActivity(): HomeActivity

//    @ContributesAndroidInjector(modules = arrayOf(FavoritesFragmentProvider::class))
//    abstract fun bindFavoritesFragment(): FavoritesFragment

}