package com.example.fsoft.moviestore;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.example.fsoft.moviestore.managers.DataManager;
import com.example.fsoft.moviestore.utils.rx.SchedulerProvider;
import com.example.fsoft.moviestore.view_viewmodels.favorites.FavoritesItemViewModel;
import com.example.fsoft.moviestore.view_viewmodels.favorites.FavoritesViewModel;

import javax.inject.Inject;

/**
 * Created by nguyennv on 3/28/2018 AD.
 */

public class ViewModelProviderFactory<V> implements ViewModelProvider.Factory {

    private V viewModel;

    public ViewModelProviderFactory(V viewModel) {
        this.viewModel = viewModel;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {

        if (modelClass.isAssignableFrom(viewModel.getClass()))
            return (T) viewModel;
        throw new IllegalArgumentException("Unknown class name");
    }
}
