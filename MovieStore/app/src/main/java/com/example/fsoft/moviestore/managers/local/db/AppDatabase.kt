package com.example.fsoft.moviestore.managers.local.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.example.fsoft.moviestore.managers.local.db.dao.MovieDAO
import com.example.fsoft.moviestore.models.PopularMovie

@Database(entities = [(PopularMovie::class)],version = 1, exportSchema = false)
@TypeConverters(com.example.fsoft.moviestore.managers.local.db.TypeConverters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun movieDAO(): MovieDAO
}
