package com.example.fsoft.moviestore.view_viewmodels.favorites

import com.example.fsoft.moviestore.models.PopularMovie

/**
 * Created by nguyennv on 3/27/2018 AD.
 */
interface FavoritesNavigator {
    // func in favoritesviewmodel
    abstract fun handleError(throwable: Throwable)

    abstract fun updateFavorites(movieList: List<PopularMovie>)
}