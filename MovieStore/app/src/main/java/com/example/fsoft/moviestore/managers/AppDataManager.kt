

package com.example.fsoft.moviestore.managers

import android.content.Context

import com.example.fsoft.moviestore.managers.local.db.DbHelper
import com.example.fsoft.moviestore.managers.local.prefs.PreferencesHelper
import com.example.fsoft.moviestore.managers.remote.ApiHelper
import com.example.fsoft.moviestore.models.PagePopularMovie
import com.example.fsoft.moviestore.models.PopularMovie

import javax.inject.Inject
import javax.inject.Singleton

import io.reactivex.Observable

@Singleton
class AppDataManager
@Inject
constructor (val mDbHelper: DbHelper, val mPreferencesHelper: PreferencesHelper, val mApiHelper: ApiHelper) : DataManager {

    override fun getMovieDetail(): Observable<List<PopularMovie>> {
        return  mApiHelper.getMovieDetail()
    }

    override fun getListMovie(): Observable<PagePopularMovie> {
        return  mApiHelper.getListMovie()
    }

    override fun getAllFavMovie(): Observable<List<PopularMovie>> {
        return mDbHelper.getAllFavMovie()
    }

    override fun deleteMovie(popularMovie: PopularMovie) {
        mDbHelper.deleteMovie(popularMovie)
    }

    override fun addFavMovie(popularMovie: PopularMovie) {
       mDbHelper.addFavMovie(popularMovie)
    }

    override fun getFavMovie(id: Int): Observable<PopularMovie> {
        return mDbHelper.getFavMovie(id)
    }

    override fun deleteAllFavMovie() {
        mDbHelper.deleteAllFavMovie()
    }

}
