
package com.example.fsoft.moviestore.managers.remote

import com.example.fsoft.moviestore.models.PagePopularMovie
import com.example.fsoft.moviestore.models.PopularMovie
import retrofit2.Call
import io.reactivex.Observable
import java.util.*

interface ApiHelper {

    fun getMovieDetail() : Observable<List<PopularMovie>>

    fun getListMovie() : Observable<PagePopularMovie>
}
