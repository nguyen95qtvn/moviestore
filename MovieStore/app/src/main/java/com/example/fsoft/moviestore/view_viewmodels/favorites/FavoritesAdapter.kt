package com.example.fsoft.moviestore.view_viewmodels.favorites

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.PointerIcon
import android.view.View
import android.view.ViewGroup
import com.example.fsoft.moviestore.databinding.ItemFavoritesViewBinding
import com.example.fsoft.moviestore.models.PopularMovie
import com.example.fsoft.moviestore.view_viewmodels.base.BaseViewHolder

/**
 * Created by nguyennv on 3/27/2018 AD.
 */
class FavoritesAdapter(var mMovieList: List<PopularMovie>): RecyclerView.Adapter<BaseViewHolder>() {
    var mListener: FavoritesApdapterListener? = null


    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): BaseViewHolder {
        val favoritesViewBiding = ItemFavoritesViewBinding.inflate(LayoutInflater.from(parent!!.context))
        return FavoritesViewHolder(favoritesViewBiding)
    }

        override fun getItemCount(): Int {
            return if (!mMovieList!!.isEmpty() && mMovieList!!.size > 0) mMovieList!!.size else 1
        }

        override fun onBindViewHolder(holder: BaseViewHolder?, position: Int) {
            holder!!.onBind(position)
    }

    fun clearAll() {
        mMovieList = mutableListOf()
    }

    fun addAll(data:List<PopularMovie>) {
        mMovieList = data
        notifyDataSetChanged()
    }

    interface FavoritesApdapterListener {
        fun onRetryClick()
    }

    inner class FavoritesViewHolder(val mBinding: ItemFavoritesViewBinding): BaseViewHolder(mBinding.root), FavoritesItemViewModel.FavoritesItemViewModelListener {
        var mFavoritesItemViewModel: FavoritesItemViewModel? = null


        override fun onItemClick(popularMovie: PopularMovie) {
            mListener!!.onRetryClick()
        }

        override fun onBind(position: Int) {
            if(mMovieList.isNotEmpty()) {
                val movie = mMovieList[position]

                mFavoritesItemViewModel = FavoritesItemViewModel(movie, this)
                mBinding.viewModel = mFavoritesItemViewModel

                // Immediate Binding
                // When a variable or observable changes, the binding will be scheduled to change before
                // the next frame. There are times, however, when binding must be executed immediately.
                // To force execution, use the executePendingBindings() method.
                mBinding.executePendingBindings()
            }
        }

    }
}