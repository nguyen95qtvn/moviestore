package com.example.fsoft.moviestore.di.module

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import com.example.fsoft.moviestore.managers.AppDataManager
import com.example.fsoft.moviestore.managers.DataManager
import com.example.fsoft.moviestore.managers.info.DatabaseInfo
import com.example.fsoft.moviestore.managers.info.PreferenceInfo
import com.example.fsoft.moviestore.managers.local.db.AppDatabase
import com.example.fsoft.moviestore.managers.local.db.AppDbHelper
import com.example.fsoft.moviestore.managers.local.db.DbHelper
import com.example.fsoft.moviestore.managers.local.prefs.AppPreferencesHelper
import com.example.fsoft.moviestore.managers.local.prefs.PreferencesHelper
import com.example.fsoft.moviestore.managers.remote.ApiHelper
import com.example.fsoft.moviestore.managers.remote.AppApiHelper
import com.example.fsoft.moviestore.utils.AppConstants
import com.example.fsoft.moviestore.utils.rx.AppSchedulerProvider
import com.example.fsoft.moviestore.utils.rx.SchedulerProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by nguyennv on 3/28/2018 AD.
 */
@Module
class AppModule {

    @Provides
    @Singleton
    fun provideAppDatabase(@DatabaseInfo dbName: String, context: Context): AppDatabase {
        return Room.databaseBuilder<AppDatabase>(context,AppDatabase::class.java,dbName).fallbackToDestructiveMigration().build()
    }
    @Provides
    @Singleton
    fun provideContext(application: Application): Context {
        return application
    }

    @Provides
    @Singleton
    fun provideDataManager(appDataManager: AppDataManager): DataManager {
        return appDataManager
    }

    @Provides
    @DatabaseInfo
    fun provideDatabaseName(): String {
        return AppConstants.DB_NAME
    }

    @Provides
    @Singleton
    fun provideDbHelper(appDbHelper: AppDbHelper): DbHelper {
        return appDbHelper
    }

    @Provides
    @Singleton
    fun provideAPIHelper(appApiHelper: AppApiHelper): ApiHelper {
        return appApiHelper
    }

    @Provides
    @PreferenceInfo
    fun providePreferenceName(): String {
        return AppConstants.PREF_NAME
    }

    @Provides
    @Singleton
    fun providePreferencesHelper(appPreferencesHelper: AppPreferencesHelper): PreferencesHelper {
        return appPreferencesHelper
    }


    @Provides
    @Singleton
    fun provideSchedulerProvider(): SchedulerProvider {
        return AppSchedulerProvider()
    }
//    @Provides
//    @Singleton
//    fun provideViewModelProviderFactory(dataManager: DataManager, schedulerProvider: SchedulerProvider):ViewModelProviderFactory {
//        return ViewModelProviderFactory(dataManager, schedulerProvider)
//    }
//
//    @Binds
//    internal abstract fun provideViewModelProviderFactory():ViewModelProviderFactory
}