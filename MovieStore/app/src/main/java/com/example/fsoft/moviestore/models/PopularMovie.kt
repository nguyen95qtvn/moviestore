package com.example.fsoft.moviestore.models

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



/**
 * Created by HuuMX1 on 3/26/2018.
 */
@Entity(tableName = "FavoritesMovie")
class PopularMovie {
    @SerializedName("vote_count")
    @Expose
    @ColumnInfo(name = "voteCount")
    var voteCount: Int? = null
    @SerializedName("id")
    @Expose
    @PrimaryKey
    @ColumnInfo(name = "movieID")
    var id: Int? = null
    @SerializedName("video")
    @Expose
    @ColumnInfo(name = "video")
    var video: Boolean? = null
    @SerializedName("vote_average")
    @Expose
    @ColumnInfo(name = "voteAverage")
    var voteAverage: Double? = null
    @SerializedName("title")
    @Expose
    @ColumnInfo(name = "title")
    var title: String? = null
    @SerializedName("popularity")
    @Expose
    @ColumnInfo(name = "popularity")
    var popularity: Double? = null
    @SerializedName("poster_path")
    @Expose
    @ColumnInfo(name = "posterpath")
    var posterPath: String? = null
    @SerializedName("original_language")
    @Expose
    @ColumnInfo(name = "originalLanguage")
    var originalLanguage: String? = null
    @SerializedName("original_title")
    @Expose
    @ColumnInfo(name = "originalTitle")
    var originalTitle: String? = null

    @ColumnInfo(name = "genreIds")
    @SerializedName("genre_ids")
    @Expose
    var genreIds: List<Int>? = null

    @SerializedName("backdrop_path")
    @Expose
    @ColumnInfo(name = "backdropPath")
    var backdropPath: String? = null
    @SerializedName("adult")
    @Expose
    @ColumnInfo(name = "adult")
    var adult: Boolean? = null
    @SerializedName("overview")
    @Expose
    @ColumnInfo(name = "overview")
    var overview: String? = null
    @SerializedName("release_date")
    @Expose
    @ColumnInfo(name = "releaseDate")
    var releaseDate: String? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PopularMovie

        if (voteCount != other.voteCount) return false
        if (id != other.id) return false
        if (video != other.video) return false
        if (voteAverage != other.voteAverage) return false
        if (title != other.title) return false
        if (popularity != other.popularity) return false
        if (posterPath != other.posterPath) return false
        if (originalLanguage != other.originalLanguage) return false
        if (originalTitle != other.originalTitle) return false
        if (backdropPath != other.backdropPath) return false
        if (adult != other.adult) return false
        if (overview != other.overview) return false
        if (releaseDate != other.releaseDate) return false

        return true
    }

    override fun hashCode(): Int {
        var result = voteCount ?: 0
        result = 31 * result + (id ?: 0)
        result = 31 * result + (video?.hashCode() ?: 0)
        result = 31 * result + (voteAverage?.hashCode() ?: 0)
        result = 31 * result + (title?.hashCode() ?: 0)
        result = 31 * result + (popularity?.hashCode() ?: 0)
        result = 31 * result + (posterPath?.hashCode() ?: 0)
        result = 31 * result + (originalLanguage?.hashCode() ?: 0)
        result = 31 * result + (originalTitle?.hashCode() ?: 0)
        result = 31 * result + (backdropPath?.hashCode() ?: 0)
        result = 31 * result + (adult?.hashCode() ?: 0)
        result = 31 * result + (overview?.hashCode() ?: 0)
        result = 31 * result + (releaseDate?.hashCode() ?: 0)
        return result
    }


}