
package com.example.fsoft.moviestore.managers.local.db

import io.reactivex.Observable
import com.example.fsoft.moviestore.models.PopularMovie


interface DbHelper {
    fun getAllFavMovie(): Observable<List<PopularMovie>>
    fun deleteMovie(popularMovie: PopularMovie)
    fun addFavMovie(popularMovie: PopularMovie)
    fun getFavMovie(id: Int): Observable<PopularMovie>
    fun deleteAllFavMovie()
}
