package com.example.fsoft.moviestore.managers.local.db;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;

public class TypeConverters {
    @TypeConverter
    public static Date fromTimestamp(Long value) {
        return value == null ? null : new Date(value);
    }

    @TypeConverter
    public static Long dateToTimestamp(Date date) {
        if (date == null) {
            return null;
        } else {
            return date.getTime();
        }
    }

    @TypeConverter
    public static List<Integer> fromListInteger(String value) {
        Type listType = new TypeToken<List<Integer>>() {}.getType();
        return value == null ? null : (List<Integer>) new Gson().fromJson(value, listType);
    }

    @TypeConverter
    public static String ListIntegerToJson(List<Integer> list) {
        if (list == null) {
            return null;
        } else {
            return new Gson().toJson(list);
        }
    }
}
